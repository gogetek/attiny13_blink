//input

#define a0_input DDRA &= ~_BV(PA0)
#define a1_input DDRA &= ~_BV(PA1)
#define a2_input DDRA &= ~_BV(PA2)
#define a3_input DDRA &= ~_BV(PA3)
#define a4_input DDRA &= ~_BV(PA4)
#define a5_input DDRA &= ~_BV(PA5)
#define a6_input DDRA &= ~_BV(PA6)
#define a7_input DDRA &= ~_BV(PA7)

#define b0_input DDRB &= ~_BV(PB0)
#define b1_input DDRB &= ~_BV(PB1)
#define b2_input DDRB &= ~_BV(PB2)
#define b3_input DDRB &= ~_BV(PB3)
#define b4_input DDRB &= ~_BV(PB4)
#define b5_input DDRB &= ~_BV(PB5)
#define b6_input DDRB &= ~_BV(PB6)
#define b7_input DDRB &= ~_BV(PB7)

#define c0_input DDRC &= ~_BV(PC0)
#define c1_input DDRC &= ~_BV(PC1)
#define c2_input DDRC &= ~_BV(PC2)
#define c3_input DDRC &= ~_BV(PC3)
#define c4_input DDRC &= ~_BV(PC4)
#define c5_input DDRC &= ~_BV(PC5)
#define c6_input DDRC &= ~_BV(PC6)
#define c7_input DDRC &= ~_BV(PC7)

#define d0_input DDRD &= ~_BV(PD0)
#define d1_input DDRD &= ~_BV(PD1)
#define d2_input DDRD &= ~_BV(PD2)
#define d3_input DDRD &= ~_BV(PD3)
#define d4_input DDRD &= ~_BV(PD4)
#define d5_input DDRD &= ~_BV(PD5)
#define d6_input DDRD &= ~_BV(PD6)
#define d7_input DDRD &= ~_BV(PD7)

#define e0_input DDRE &= ~_BV(PE0)
#define e1_input DDRE &= ~_BV(PE1)
#define e2_input DDRE &= ~_BV(PE2)
#define e3_input DDRE &= ~_BV(PE3)
#define e4_input DDRE &= ~_BV(PE4)
#define e5_input DDRE &= ~_BV(PE5)
#define e6_input DDRE &= ~_BV(PE6)
#define e7_input DDRE &= ~_BV(PE7)

//output

#define a0_output DDRA|= _BV(PA0)
#define a1_output DDRA|= _BV(PA1)
#define a2_output DDRA|= _BV(PA2)
#define a3_output DDRA|= _BV(PA3)
#define a4_output DDRA|= _BV(PA4)
#define a5_output DDRA|= _BV(PA5)
#define a6_output DDRA|= _BV(PA6)
#define a7_output DDRA|= _BV(PA7)

#define b0_output DDRB|= _BV(PB0)
#define b1_output DDRB|= _BV(PB1)
#define b2_output DDRB|= _BV(PB2)
#define b3_output DDRB|= _BV(PB3)
#define b4_output DDRB|= _BV(PB4)
#define b5_output DDRB|= _BV(PB5)
#define b6_output DDRB|= _BV(PB6)
#define b7_output DDRB|= _BV(PB7)

#define c0_output DDRC|= _BV(PC0)
#define c1_output DDRC|= _BV(PC1)
#define c2_output DDRC|= _BV(PC2)
#define c3_output DDRC|= _BV(PC3)
#define c4_output DDRC|= _BV(PC4)
#define c5_output DDRC|= _BV(PC5)
#define c6_output DDRC|= _BV(PC6)
#define c7_output DDRC|= _BV(PC7)

#define d0_output DDRD|= _BV(PD0)
#define d1_output DDRD|= _BV(PD1)
#define d2_output DDRD|= _BV(PD2)
#define d3_output DDRD|= _BV(PD3)
#define d4_output DDRD|= _BV(PD4)
#define d5_output DDRD|= _BV(PD5)
#define d6_output DDRD|= _BV(PD6)
#define d7_output DDRD|= _BV(PD7)

#define e0_output DDRE|= _BV(PE0)
#define e1_output DDRE|= _BV(PE1)
#define e2_output DDRE|= _BV(PE2)
#define e3_output DDRE|= _BV(PE3)
#define e4_output DDRE|= _BV(PE4)
#define e5_output DDRE|= _BV(PE5)
#define e6_output DDRE|= _BV(PE6)
#define e7_output DDRE|= _BV(PE7)

//high

#define a0_high PORTA|= _BV(PA0)
#define a1_high PORTA|= _BV(PA1)
#define a2_high PORTA|= _BV(PA2)
#define a3_high PORTA|= _BV(PA3)
#define a4_high PORTA|= _BV(PA4)
#define a5_high PORTA|= _BV(PA5)
#define a6_high PORTA|= _BV(PA6)
#define a7_high PORTA|= _BV(PA7)

#define b0_high PORTB|= _BV(PB0)
#define b1_high PORTB|= _BV(PB1)
#define b2_high PORTB|= _BV(PB2)
#define b3_high PORTB|= _BV(PB3)
#define b4_high PORTB|= _BV(PB4)
#define b5_high PORTB|= _BV(PB5)
#define b6_high PORTB|= _BV(PB6)
#define b7_high PORTB|= _BV(PB7)

#define c0_high PORTC|= _BV(PC0)
#define c1_high PORTC|= _BV(PC1)
#define c2_high PORTC|= _BV(PC2)
#define c3_high PORTC|= _BV(PC3)
#define c4_high PORTC|= _BV(PC4)
#define c5_high PORTC|= _BV(PC5)
#define c6_high PORTC|= _BV(PC6)
#define c7_high PORTC|= _BV(PC7)

#define d0_high PORTD|= _BV(PD0)
#define d1_high PORTD|= _BV(PD1)
#define d2_high PORTD|= _BV(PD2)
#define d3_high PORTD|= _BV(PD3)
#define d4_high PORTD|= _BV(PD4)
#define d5_high PORTD|= _BV(PD5)
#define d6_high PORTD|= _BV(PD6)
#define d7_high PORTD|= _BV(PD7)

#define e0_high PORTE|= _BV(PE0)
#define e1_high PORTE|= _BV(PE1)
#define e2_high PORTE|= _BV(PE2)
#define e3_high PORTE|= _BV(PE3)
#define e4_high PORTE|= _BV(PE4)
#define e5_high PORTE|= _BV(PE5)
#define e6_high PORTE|= _BV(PE6)
#define e7_high PORTE|= _BV(PE7)

//low

#define a0_low PORTA &= ~_BV(PA0)
#define a1_low PORTA &= ~_BV(PA1)
#define a2_low PORTA &= ~_BV(PA2)
#define a3_low PORTA &= ~_BV(PA3)
#define a4_low PORTA &= ~_BV(PA4)
#define a5_low PORTA &= ~_BV(PA5)
#define a6_low PORTA &= ~_BV(PA6)
#define a7_low PORTA &= ~_BV(PA7)

#define b0_low PORTB &= ~_BV(PB0)
#define b1_low PORTB &= ~_BV(PB1)
#define b2_low PORTB &= ~_BV(PB2)
#define b3_low PORTB &= ~_BV(PB3)
#define b4_low PORTB &= ~_BV(PB4)
#define b5_low PORTB &= ~_BV(PB5)
#define b6_low PORTB &= ~_BV(PB6)
#define b7_low PORTB &= ~_BV(PB7)

#define c0_low PORTC &= ~_BV(PC0)
#define c1_low PORTC &= ~_BV(PC1)
#define c2_low PORTC &= ~_BV(PC2)
#define c3_low PORTC &= ~_BV(PC3)
#define c4_low PORTC &= ~_BV(PC4)
#define c5_low PORTC &= ~_BV(PC5)
#define c6_low PORTC &= ~_BV(PC6)
#define c7_low PORTC &= ~_BV(PC7)

#define d0_low PORTD &= ~_BV(PD0)
#define d1_low PORTD &= ~_BV(PD1)
#define d2_low PORTD &= ~_BV(PD2)
#define d3_low PORTD &= ~_BV(PD3)
#define d4_low PORTD &= ~_BV(PD4)
#define d5_low PORTD &= ~_BV(PD5)
#define d6_low PORTD &= ~_BV(PD6)
#define d7_low PORTD &= ~_BV(PD7)

#define e0_low PORTE &= ~_BV(PE0)
#define e1_low PORTE &= ~_BV(PE1)
#define e2_low PORTE &= ~_BV(PE2)
#define e3_low PORTE &= ~_BV(PE3)
#define e4_low PORTE &= ~_BV(PE4)
#define e5_low PORTE &= ~_BV(PE5)
#define e6_low PORTE &= ~_BV(PE6)
#define e7_low PORTE &= ~_BV(PE7)

//get

#define get_a0 PINA & 1 << 0
#define get_a1 PINA & 1 << 1
#define get_a2 PINA & 1 << 2
#define get_a3 PINA & 1 << 3
#define get_a4 PINA & 1 << 4
#define get_a5 PINA & 1 << 5
#define get_a6 PINA & 1 << 6
#define get_a7 PINA & 1 << 7

#define get_b0 PINB & 1 << 0
#define get_b1 PINB & 1 << 1
#define get_b2 PINB & 1 << 2
#define get_b3 PINB & 1 << 3
#define get_b4 PINB & 1 << 4
#define get_b5 PINB & 1 << 5
#define get_b6 PINB & 1 << 6
#define get_b7 PINB & 1 << 7

#define get_c0 PINC & 1 << 0
#define get_c1 PINC & 1 << 1
#define get_c2 PINC & 1 << 2
#define get_c3 PINC & 1 << 3
#define get_c4 PINC & 1 << 4
#define get_c5 PINC & 1 << 5
#define get_c6 PINC & 1 << 6
#define get_c7 PINC & 1 << 7

#define get_d0 PIND & 1 << 0
#define get_d1 PIND & 1 << 1
#define get_d2 PIND & 1 << 2
#define get_d3 PIND & 1 << 3
#define get_d4 PIND & 1 << 4
#define get_d5 PIND & 1 << 5
#define get_d6 PIND & 1 << 6
#define get_d7 PIND & 1 << 7

#define get_e0 PINE & 1 << 0
#define get_e1 PINE & 1 << 1
#define get_e2 PINE & 1 << 2
#define get_e3 PINE & 1 << 3
#define get_e4 PINE & 1 << 4
#define get_e5 PINE & 1 << 5
#define get_e6 PINE & 1 << 6
#define get_e7 PINE & 1 << 7

//switch

#define switch_a0 PORTA ^= (1 << 0)
#define switch_a1 PORTA ^= (1 << 1)
#define switch_a2 PORTA ^= (1 << 2)
#define switch_a3 PORTA ^= (1 << 3)
#define switch_a4 PORTA ^= (1 << 4)
#define switch_a5 PORTA ^= (1 << 5)
#define switch_a6 PORTA ^= (1 << 6)
#define switch_a7 PORTA ^= (1 << 7)

#define switch_b0 PORTB ^= (1 << 0)
#define switch_b1 PORTB ^= (1 << 1)
#define switch_b2 PORTB ^= (1 << 2)
#define switch_b3 PORTB ^= (1 << 3)
#define switch_b4 PORTB ^= (1 << 4)
#define switch_b5 PORTB ^= (1 << 5)
#define switch_b6 PORTB ^= (1 << 6)
#define switch_b7 PORTB ^= (1 << 7)

#define switch_c0 PORTC ^= (1 << 0)
#define switch_c1 PORTC ^= (1 << 1)
#define switch_c2 PORTC ^= (1 << 2)
#define switch_c3 PORTC ^= (1 << 3)
#define switch_c4 PORTC ^= (1 << 4)
#define switch_c5 PORTC ^= (1 << 5)
#define switch_c6 PORTC ^= (1 << 6)
#define switch_c7 PORTC ^= (1 << 7)

#define switch_d0 PORTD ^= (1 << 0)
#define switch_d1 PORTD ^= (1 << 1)
#define switch_d2 PORTD ^= (1 << 2)
#define switch_d3 PORTD ^= (1 << 3)
#define switch_d4 PORTD ^= (1 << 4)
#define switch_d5 PORTD ^= (1 << 5)
#define switch_d6 PORTD ^= (1 << 6)
#define switch_d7 PORTD ^= (1 << 7)

#define switch_e0 PORTE ^= (1 << 0)
#define switch_e1 PORTE ^= (1 << 1)
#define switch_e2 PORTE ^= (1 << 2)
#define switch_e3 PORTE ^= (1 << 3)
#define switch_e4 PORTE ^= (1 << 4)
#define switch_e5 PORTE ^= (1 << 5)
#define switch_e6 PORTE ^= (1 << 6)
#define switch_e7 PORTE ^= (1 << 7)
