Creates rectangular signal on pin B0 with frequency regulation on pins B1 and B2.
The pin_macros.h file makes life easier, so I really recommend to put it in your lib directory.
