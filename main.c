//Chip: ATtiny13

#define F_CPU 1000000

#include "avr/io.h"
#include "../lib/pin_macros.h"
#include "stdbool.h"

void blink(int f)
{
	int timer_limit;
	timer_limit = F_CPU / f / 2 / 1024;		// sets timer limit
	
	if(TCNT0 > timer_limit)
	{
		switch_b0;							// switches B0 state
		TCNT0 = 0;							// reset timer
	}
}

int main()
{
	// Init
	b0_output;								// led output
	b1_input;								// decrease frequency
	b2_input;								// increase frequency
	
	TCCR0B |= (1 << CS00);					// sets up timer with
	TCCR0B |= (1 << CS02);					// 1024 prescaler
	
	int f;
	f = 4;									// starting frequency
	
	bool frame_b1[2] = {0};					// pair of bools saving 
	bool frame_b2[2] = {0};					// b1 and b2 states
	
	while(true)
	{
		// Write data to frames
		frame_b1[0] = frame_b1[1];			// shift data 
		frame_b2[0] = frame_b2[1];
		
		frame_b1[1] = get_b1;				// save new states
		frame_b2[1] = get_b2;
		
		// Check if button is pressed 
		if(frame_b1[1] < frame_b1[0] && f > 4)
			f--;
		if(frame_b2[1] < frame_b2[0])
			f++;
			
		blink(f);
	}
}
